<?php

namespace Catgolin\WebPenguin\DataFixtures\Processor;

use Catgolin\WebPenguin\Entity\Content\ContentType\User;
use Fidry\AliceDataFixtures\ProcessorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserProcessor implements ProcessorInterface
{
    protected $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function preProcess(string $fixtureId, $object) : void
    {
        if(!$object instanceof User) {
            return;
        } else {
            $password = $this->encoder
                ->encodePassword($object, $object->getPassword());
            $object->setPassword($password);
        }
    }

    public function postProcess(string $id, $object) : void
    {
        return;
    }
}
