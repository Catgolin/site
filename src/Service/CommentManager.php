<?php

namespace Catgolin\WebPenguin\Service;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentTree\Relation;
use Catgolin\WebPenguin\Entity\Content\ContentType\Comment;
use Catgolin\WebPenguin\Entity\Content\Link;
use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Form\CommentType;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;

class CommentManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }
    public function manageForm(Form $form,
        Node $author,
        ContentVersion $parent,
        bool $creation = true): ?Comment
    {
        if($form->isSubmitted() && $form->isValid()) {
            $comment = $form->getData();
            $comment->setAuthor($author);
            $comment->setPublishedAt(new \DateTime());
            $comment->setParent($parent);
            if($creation === true) {
                $node = new Node();
                $node->setIsDraft(false);
                $node->setOwner($author);
                $node->addContent($comment);
                $relation = new Relation($parent->getNode(), $node);
                $this->entityManager->persist($node);
                $this->entityManager->persist($relation);
            }
            $this->entityManager->persist($comment);
            $this->entityManager->flush();
            return $comment;
        }
        return null;
    }
}
