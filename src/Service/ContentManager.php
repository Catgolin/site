<?php

namespace Catgolin\WebPenguin\Service;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentTree\Relation;
use Catgolin\WebPenguin\Entity\Content\ContentTree\Root;
use Catgolin\WebPenguin\Entity\Content\ContentVersion;

use Doctrine\ORM\EntityManagerInterface;

class ContentManager
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Persists a new node for the corresponding content.
     * @param ContentVersion $new: the content to create
     * @param Node $author: the authority creating the content
     * @param Node $parent: if not null, a relation will be created
     * between the new content and this node (default: null)
     * @param bool $isDraft: if true, the node will be saved
     * but not marked as published (default: false)
     * @param bool $flush: if true, the entity manager will flush the modifications
     * to the database (default: true)
     * @return ContentVersion: the created content.
    */
    public function create(
        ContentVersion $content,
        Node $author,
        ?Node $parent,
        bool $isDraft = false,
        bool $flush = true
        ): ContentVersion
    {
        $node = $parent === null ? new Root() : new Node();
        $node->setIsDraft($isDraft);
        $node->setOwner($author);
        $node->addContent($content);
        $this->entityManager->persist($node);
        if($parent !== null)
            $this->entityManager->persist(new Relation($parent, $node));
        $this->persistContent($content, $author, $isDraft, false);
        if($flush) $this->entityManager->flush();
        return $content;
    }

    /**
     * Persists a new version of a content
     * @param ContentVersion $origin: the original content that has to be replaced
     * @param ContentVersion $new: the new content to persist
     * @param Node $author: the authority creatubg the new content
     * @param bool $isDraft: if true, the node will be saved but not marked
     * as published (default: false)
     * @param bool $flush: if true, the entity manager will flush the modifications
     * to the database (default: true)
     * @return ContentVersion: the created content.
     */
    public function update(
        ContentVersion $origin,
        ContentVersion $new,
        Node $author,
        bool $isDraft = false,
        bool $flush = true
        ): ContentVersion
    {
        $new->setOrigin($origin);
        if(!$isDraft) $origin->setArchivedAt(new \DateTime);
        $origin->getNode()->addContent($new);
        return $this->persistContent($new, $author, $isDraft, $flush);
    }

    private function persistContent(
        ContentVersion $content,
        Node $author,
        bool $isDraft = false,
        bool $flush = true
        ): ContentVersion
    {
        $content->setAuthor($author);
        $content->setCreatedAt(new \DateTime());
        if(!$isDraft) $content->setPublishedAt(new \DateTime());
        $this->entityManager->persist($content);
        if($flush) $this->entityManager->flush();
        return $content;
    }
}
