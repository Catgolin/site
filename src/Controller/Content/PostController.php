<?php

namespace Catgolin\WebPenguin\Controller\Content;

use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Form\EditPostType;

use Symfony\Component\Form\Form;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/posts", name="post_")
 */
class PostController extends ContentController
{
    protected function getCreationForm(?ContentVersion $content = null): Form
    {
        if($content !== null) $content = clone $content;
        return $this->createForm(EditPostType::class, $content);
    }

    public function contentType(): string
    {
        return 'post';
    }
}
