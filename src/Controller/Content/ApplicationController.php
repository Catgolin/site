<?php

namespace Catgolin\WebPenguin\Controller\Content;

use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Form\EditApplicationType;
use Catgolin\WebPenguin\Form\JavaScriptEditorType;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/applications", name="application_")
 */
class ApplicationController extends ContentController
{
    protected function getCreationForm(?ContentVersion $content = null): Form
    {
        if($content !== null) $content = clone $content;
        return $this->createForm(EditApplicationType::class, $content);
    }

    protected function getEditionForms(ContentVersion $content): array
    {
        $self = $this;
        $forms = [
            "edit_application" => function() use($self, $content) {
                return $self->getCreationForm($content);
            },
            "create_java_script_file" => function() use($self, $content) {
                return $self->get('form.factory')->createNamed(
                    'create_java_script_file',
                    JavaScriptEditorType::class
                );
            },
        ];
        foreach($content->getCode() as $node) {
            $file = $node->getCurrent();
            $key = "edit_java_script_file_" . $file->getTitle();
            $forms[$key] = function() use($self, $key, $file) {
                $form = $self->get('form.factory')
                    ->createNamed(
                        $key,
                        JavaScriptEditorType::class,
                        clone $file
                    )
                ;
                return $form;
            };
        }
        return $forms;
    }

    protected function manageEdition(
        string $key,
        Form $form,
        ContentVersion $origin
    ): Response
    {
        if($key === "edit_application") {
            return parent::manageEdition($key, $form, $origin);
        } elseif($key === "create_java_script_file") {
            $file = $this->contentManager->create(
                $form->getData(),
                $this->getUser()->getNode(),
                $origin->getNode(),
                false,
                false
            );
            $origin->addCode($file->getNode());
            $this->getDoctrine()->getManager()->persist($origin);
            $file->getNode()->setOwner($origin->getNode());
            $this->getDoctrine()->getManager()->persist($origin->getNode());
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute(
                "application_edition",
                [
                    'content' => $origin->getNode()->getDefaultLink()->getName(),
                ],
                Response::HTTP_CREATED
            );
        } else {
            $title = substr($key, strlen("edit_java_script_file_"));
            $file = $origin->getCode()
                ->filter(function($code) use($title) {
                    return $code->getCurrent()->getTitle() === $title;
                })
                ->first()
                ->getCurrent()
            ;
            $this->contentManager->update(
                $file,
                $form->getData(),
                $this->getUser()->getNode(),
                false,
                false
            );
            $file->getNode()->setOwner($origin->getNode());
            $this->getDoctrine()->getManager()->persist($origin->getNode());
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute(
                "application_edition",
                [
                    'content' => $origin->getNode()->getDefaultLink()->getName(),
                ]
            );
        }
    }

    public function contentType(): string
    {
        return "application";
    }
}
