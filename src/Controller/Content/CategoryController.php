<?php

namespace Catgolin\WebPenguin\Controller\Content;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Form\EditCategoryType;
use Catgolin\WebPenguin\Form\EditPostType;

use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/categories", name="category_")
 */
class CategoryController extends ContentController
{
    protected function getCreationForm(?ContentVersion $content = null): Form
    {
        if($content !== null) $content = clone $content;
        return $this->createForm(EditCategoryType::class, $content);
    }

    protected function getEditionForms(ContentVersion $content): array
    {
        $self = $this;
        $forms = [
            "edit_category" => function() use ($self, $content) {
                return $self->getCreationForm($content);
            },
            "create_post" => function() use ($self) {
                return $self->createForm(EditPostType::class);
            },
        ];
        return $forms;
    }

    protected function manageEdition(
        string $key,
        Form $form,
        ContentVersion $origin
    ): Response
    {
        if($key === "edit_category") {
            return parent::manageEdition($key, $form, $origin);
        } else {
            return parent::manageCreation($form, $origin->getNode());
        }
    }

    public function contentType(): string
    {
        return 'category';
    }
}
