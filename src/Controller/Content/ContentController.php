<?php

namespace Catgolin\WebPenguin\Controller\Content;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\Link;
use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Form\CommentType;
use Catgolin\WebPenguin\Security\Voter\ContentActionVoter;
use Catgolin\WebPenguin\Service\ContentManager;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Form;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

abstract class ContentController extends AbstractController
{
    protected $translator;
    protected $contentManager;

    public function __construct(TranslatorInterface $t, ContentManager $cm)
    {
        $this->translator = $t;
        $this->contentManager = $cm;
    }

    /**
     * @Route("/c/{parent}", name="creation")
     * @Entity("parentLink", expr="repository.findOneByName(parent)")
     * @IsGranted("ROLE_USER")
     */
    public function create(Request $request, ?Link $parentLink): Response
    {
        $parent = $parentLink === null ? null : $parentLink->getTarget();
        if($parent !== null) {
            $this->denyAccessUnlessGranted(ContentActionVoter::EDIT, $parent);
        }
        $form = $this->getCreationForm();
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            return $this->manageCreation($form, $parent);
        }
        return $this->displayCreationPage($form, $parent);
    }

    /**
     * @Route("/c", name="to_root")
     * @Isgranted("ROLE_ADMIN")
     */
    public function createRoot(Request $request): Response
    {
        return $this->create($request, null);
    }

    /**
     * @Route("/e/{content}", name="edition")
     * @Entity("contentLink", expr="repository.findOneByName(content)")
     * @IsGranted("ROLE_USER")
     */
    public function edit(Request $request, Link $contentLink): Response
    {
        $content = $contentLink->getTarget()->getCurrent();
        $this->denyAccessUnlessGranted(ContentActionVoter::EDIT, $content->getNode());
        $formBuilders = $this->getEditionForms($content);
        $forms = [];
        foreach($formBuilders as $key => $formBuilder) {
            $form = $formBuilder();
            $forms[$key] = $form;
            $form->handleRequest($request);
            if($form->isSubmitted() && $form->isValid()) {
                return $this->manageEdition(
                    $key,
                    $form,
                    $content
                );
            }
        }
        return $this->displayEditionPage(
            $forms,
            $content
        );
    }

    /**
     * @Route("/v/{content}", name="view")
     * @Entity("contentLink", expr="repository.findOneByName(content)")
     */
    public function view(Request $request, Link $contentLink): Response
    {
        $node = $contentLink->getTarget();
        $content = $node->getCurrent();
        $commentForm = $this->createForm(CommentType::class);
        $commentForm->handleRequest($request);
        if($commentForm->isSubmitted() && $commentForm->isValid()) {
            $comment = $this->contentManager->create(
                $commentForm->getData()->setParent($node->getCurrent()),
                $this->getUser()->getNode(),
                $contentLink->getTarget()
            );
            $content->addComment($comment);
            return $this->redirectToRoute(
                $this->contentType() . "_view",
                [
                    "content" => $contentLink->getName(),
                ],
                Response::HTTP_CREATED
            );
        }
        return $this->displayViewPage($content, $commentForm);
    }

    /**
     * @return Form: the form to display on the creation page
     */
    protected abstract function getCreationForm(?ContentVersion $content = null): Form;

    /**
     * @return array|callback|Form: list of forms used in the edition page
     */
    protected function getEditionForms(ContentVersion $content): array
    {
        $self = $this;
        return [
            "edit_" . $this->contentType() => function() use ($self, $content) {
                return $this->getCreationForm($content);
            },
        ];
    }

    /**
     * @param Form $form:
     * @param Node $parent:
     */
    protected function manageCreation(Form $form, ?Node $parent): Response
    {
        $content = $this->contentManager->create(
            $form->getData(),
            $this->getUser()->getNode(),
            $parent
        );
        return $this->redirectToRoute(
            $content->getType() . "_edition",
            [
                "content" => $content->getNode()->getDefaultLink()->getName()
            ],
            Response::HTTP_CREATED
        );
    }

    protected function manageEdition(
        string $key,
        Form $form,
        ContentVersion $origin
    ): Response
    {
        $content = $this->contentManager->update(
            $origin,
            $form->getData(),
            $this->getUser()->getNode()
        );
        return $this->redirectToRoute($this->contentType() . "_view", [
            "content" => $origin->getNode()->getDefaultLink()->getName()
        ]);
    }

    protected function displayCreationPage(
        Form $form,
        ?Node $parent
    ): Response
    {
        return $this->render(
            'content/' . $this->contentType() . '/creation.html.twig',
            [
                'form' => $form->createView(),
                'parent' => $parent
            ]
        );
    }

    protected function displayEditionPage(
        array $forms,
        ContentVersion $content
    ): Response
    {
        return $this->render(
            'content/' . $this->contentType() . '/edition.html.twig',
            [
                'forms' => array_map(
                    function($form) {
                        return $form->createView();
                    },
                    $forms
                ),
                'content' => $content,
            ]
        );
    }

    protected function displayViewPage(
        ContentVersion $content,
        Form $commentForm
    ): Response
    {
        return $this->render(
            'content/' . $this->contentType() . '/view.html.twig',
            [
                'comment_form' => $commentForm->createView(),
                'content' => $content
            ]
        );
    }

    public abstract function contentType(): string;
}
