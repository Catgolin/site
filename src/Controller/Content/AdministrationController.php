<?php

namespace Catgolin\WebPenguin\Controller\Content;

use Catgolin\WebPenguin\Entity\Content\Link;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Entity;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class AdministrationController extends AbstractController
{
    /**
     * @Route("/admin/{content}", name="content_administration")
     * @Entity("link", expr="repository.findOneByName(content)")
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(Link $link): Response
    {
        return $this->render(
            'content/administration/administration.html.twig',
            [
                'content' => $link->getTarget(),
            ]
        );
    }
}
