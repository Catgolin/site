<?php

namespace Catgolin\WebPenguin\Controller;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentType\File;
use Catgolin\WebPenguin\Entity\Content\Link;
use Catgolin\WebPenguin\Form\FileUploaderType;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class FilesController extends AbstractController
{
    /**
     * @Route("/c/files", name="files")
     * @IsGranted("ROLE_USER")
     */
    public function uploadFile(Request $request, SluggerInterface $slugger, TranslatorInterface $translator): Response
    {
        $form = $this->createForm(FileUploaderType::class);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()) {
            $this->create($slugger, $translator, $form);
            $this->addFlash('success',
                $translator->trans('flash.file.uploaded')
            );
            return $this->redirectToRoute('home', [], Response::HTTP_CREATED);
        }
        return $this->render('files/create.html.twig', [
            'uploader' => $form->createView()
        ]);
    }

    public function upload(SluggerInterface $slugger, TranslatorInterface $translator, Form $form): File {
        $entityManager = $this->getDoctrine()->getManager();
        // Create the node
        $node = new Node();
        $node->setIsDraft(false);
        $node->setOwner($this->getUser()->getNode());
        $entityManager->persist($node);
        // Upload the file
        $realFile = $form->get('file')->getData();
        $fileName = $slugger->slug(
            pathinfo($realFile->getClientOriginalName(), PATHINFO_FILENAME)
        ) . '-' . uniqid() . '.' . $realFile->guessExtension();
        try {
            $realFile->move(
                $this->getParameter('files_directory'),
                $fileName
            );
        } catch(FileException $e) {
            throw $e;
        }
        // Create the file
        $file = $form->getData();
        $node->addContent($file);
        $file->setAuthor($this->getUser()->getNode());
        $file->setPublishedAt(new \DateTime());
        $file->setFileName($fileName);
        $entityManager->persist($file);
        $entityManager->flush();
        return $file;
    }
}
