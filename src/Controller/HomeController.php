<?php

namespace Catgolin\WebPenguin\Controller;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Root;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(): Response
    {
        $roots = (new ArrayCollection(
            $this->getDoctrine()
                ->getManager()
                ->getRepository(Root::class)
                ->findAll()
            ))
            ->map(function($node) {
                return $node->getCurrent();
            })
        ;
        return $this->render('home/index.html.twig', [
            'roots' => $roots,
        ]);
    }
}
