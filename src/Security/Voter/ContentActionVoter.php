<?php

namespace Catgolin\WebPenguin\Security\Voter;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\Contentversion;

use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Authorization\Voter\Voter;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Security;

/**
 * Checks if the configuration of the node itself allows the user to edit it
 */
class ContentActionVoter extends Voter
{
    const VIEW = "view";
    const EDIT = "edit";
    const ADMIN = "admin";

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    protected function supports($attribute, $subject)
    {
        return in_array($attribute, [self::VIEW, self::EDIT, self::ADMIN])
            && $subject instanceof Node;
    }

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        if($this->security->isGranted('ROLE_ADMIN')) {
            return true;
        }

        $user = $token->getUser();
        $node = $user instanceof ContentVersion ? $user->getNode() : $user;
        if(!$node instanceof Node) {
            return false;
        }

        switch ($attribute) {
            case self::VIEW:
                return $this->canView($subject, $node);
            case self::EDIT:
                return $this->canEdit($subject, $node);
            case self::ADMIN:
                return $this->canAdmin($subject, $node);
        }

        return false;
    }

    protected function canView(Node $subject, Node $node)
    {
        if($this->canEdit($subject, $node)) {
            return true;
        }
        return !$node->isDraft();
    }

    protected function canEdit(Node $subject, Node $node)
    {
        if($this->canAdmin($subject, $node)) {
            return true;
        }
        return false;
    }

    protected function canAdmin(Node $subject, Node $node)
    {
        if($node->getId() === $subject->getOwner()->getId()) {
            return true;
        }
        return false;
    }

}
