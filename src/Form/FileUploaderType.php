<?php

namespace Catgolin\WebPenguin\Form;

use Catgolin\WebPenguin\Entity\Content\ContentType\File;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FileUploaderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('source')
            ->add('copyrights')
            ->add('description')
            ->add('file', FileType::class, [
                'label' => 'File',
                'mapped' => false,
                'required' => true
            ])
            ->add('publish', SubmitType::class, [
                'label' => 'btn.publish',
                'attr' => ['class' => 'btn-success']
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => File::class,
        ]);
    }
}
