<?php

namespace Catgolin\WebPenguin\Repository\Content\ContentType;

use Catgolin\WebPenguin\Entity\Content\ContentType\JavaScriptFile;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method JavaScriptFile|null find($id, $lockMode = null, $lockVersion = null)
 * @method JavaScriptFile|null findOneBy(array $criteria, array $orderBy = null)
 * @method JavaScriptFile[]    findAll()
 * @method JavaScriptFile[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class JavaScriptFileRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, JavaScriptFile::class);
    }

    // /**
    //  * @return JavaScriptFile[] Returns an array of JavaScriptFile objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('j.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?JavaScriptFile
    {
        return $this->createQueryBuilder('j')
            ->andWhere('j.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
