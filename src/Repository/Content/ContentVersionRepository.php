<?php

namespace Catgolin\WebPenguin\Repository\Content;

use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContentVersion|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContentVersion|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContentVersion[]    findAll()
 * @method ContentVersion[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContentVersionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContentVersion::class);
    }

    // /**
    //  * @return ContentVersion[] Returns an array of ContentVersion objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContentVersion
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
