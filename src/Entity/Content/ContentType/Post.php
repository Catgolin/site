<?php

namespace Catgolin\WebPenguin\Entity\Content\ContentType;

use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Repository\Content\ContentType\PostRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post extends ContentVersion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @codeCoverageIgnore
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getDescription() : string
    {
        $content = $this->getContent();
        $len = strlen($content);
        if($len > 100)
            $description = substr($content, 0, 97)."...";
        else
            $description = $content;
        return $description;
    }
}
