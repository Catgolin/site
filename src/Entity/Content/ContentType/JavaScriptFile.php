<?php

namespace Catgolin\WebPenguin\Entity\Content\ContentType;

use Catgolin\WebPenguin\Entity\Content\ContentType\File;
use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Repository\Content\ContentType\JavaScriptFileRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=JavaScriptFileRepository::class)
 */
class JavaScriptFile extends ContentVersion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $code;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    public function __construct()
    {
        parent::__construct();
        $this->setCode("");
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getDescription(): string
    {
        return "";
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
