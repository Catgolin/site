<?php

namespace Catgolin\WebPenguin\Entity\Content\ContentType;

use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Repository\Content\ContentType\CommentRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CommentRepository::class)
 */
class Comment extends ContentVersion
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    /**
     * @ORM\ManyToOne(targetEntity=ContentVersion::class, inversedBy="comments")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parent;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDescription() : string
    {
        $content = $this->getContent();
        $len = strlen($content);
        if($len > 100)
            $description = substr($content, 0, 97)."...";
        else
            $description = $content;
        return $description;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    public function getParent(): ?ContentVersion
    {
        return $this->parent;
    }

    public function setParent(?ContentVersion $parent): self
    {
        $this->parent = $parent;

        return $this;
    }
}
