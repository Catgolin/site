<?php

namespace Catgolin\WebPenguin\Entity\Content\ContentTree;

use Catgolin\WebPenguin\Repository\Content\ContentTree\RelationRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RelationRepository::class)
 */
class Relation
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Node::class, inversedBy="childs")
     * @ORM\JoinColumn(nullable=false)
     */
    private $parent;

    /**
     * @ORM\ManyToOne(targetEntity=Node::class, inversedBy="parents")
     * @ORM\JoinColumn(nullable=false)
     */
    private $child;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $place;

    public function __construct(Node $parent, Node $child, int $place = -1)
    {
        $this->setChild($child);
        $parent->addChild($this);
        $this->setPlace($place < 0 ? $this->getParent()->getChilds()->count() : $place);
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getParent(): ?Node
    {
        return $this->parent;
    }

    public function setParent(?Node $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChild(): ?Node
    {
        return $this->child;
    }

    public function setChild(?Node $child): self
    {
        $this->child = $child;

        return $this;
    }

    public function getPlace(): ?int
    {
        return $this->place;
    }

    public function setPlace(?int $place): self
    {
        $this->place = $place;

        return $this;
    }
}
