<?php

namespace Catgolin\WebPenguin\Entity\Content\ContentTree;

use Catgolin\WebPenguin\Entity\Content\ContentVersion;
use Catgolin\WebPenguin\Entity\Content\Link;
use Catgolin\WebPenguin\Repository\Content\ContentTree\NodeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NodeRepository::class)
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="discr", type="string")
 * @ORM\DiscriminatorMap({"node" = "Node", "root" = "Root"})
 */
class Node
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Node::class)
     */
    private $owner;

    /**
     * @ORM\OneToMany(targetEntity=Link::class, mappedBy="target", orphanRemoval=true)
     */
    private $links;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDraft;

    /**
     * @ORM\OneToMany(targetEntity=ContentVersion::class, mappedBy="node", orphanRemoval=true)
     */
    private $content;

    /**
     * @ORM\OneToOne(targetEntity=Link::class, cascade={"persist", "remove"})
     */
    private $defaultLink;

    /**
     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="parent")
     */
    private $childs;

    /**
     * @ORM\OneToMany(targetEntity=Relation::class, mappedBy="child")
     */
    private $parents;

    public function __construct()
    {
        $this->links = new ArrayCollection();
        $this->content = new ArrayCollection();
        $link = new Link($this, uniqid());
        $this->setDefaultLink($link);
        $this->addLink($link);
        $this->childs = new ArrayCollection();
        $this->parents = new ArrayCollection();
    }

    /**
     * @codeCoverageIgnore
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getOwner(): ?self
    {
        return $this->owner;
    }

    public function setOwner(?self $owner): self
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * @return Collection|Link[]
     */
    public function getLinks(): Collection
    {
        return $this->links;
    }

    public function addLink(Link $link): self
    {
        if (!$this->links->contains($link)) {
            $this->links[] = $link;
            $link->setTarget($this);
        }

        return $this;
    }

    public function removeLink(Link $link): self
    {
        if ($this->links->removeElement($link)) {
            // set the owning side to null (unless already changed)
            if ($link->getTargontentet() === $this) {
                $link->setTarget(null);
            }
        }

        return $this;
    }

    public function getIsDraft(): ?bool
    {
        return $this->isDraft;
    }

    public function setIsDraft(bool $isDraft): self
    {
        $this->isDraft = $isDraft;

        return $this;
    }

    /**
     * @return Collection|ContentVersion[]
     */
    public function getContent(): Collection
    {
        return $this->content;
    }

    public function addContent(ContentVersion $content): self
    {
        if (!$this->content->contains($content)) {
            $this->content[] = $content;
            $content->setNode($this);
        }

        return $this;
    }

    public function removeContent(ContentVersion $content): self
    {
        if ($this->content->removeElement($content)) {
            // set the owning side to null (unless already changed)
            if ($content->getNode() === $this) {
                $content->setNode(null);
            }
        }

        return $this;
    }

    public function getDefaultLink(): ?Link
    {
        return $this->defaultLink;
    }

    public function setDefaultLink(Link $defaultLink): self
    {
        $this->defaultLink = $defaultLink;

        return $this;
    }

    /**
     * @return Collection|Relation[]
     */
    public function getParents(): Collection
    {
        return $this->parents;
    }

    /**
     * @return Collection|Relation[]
     */
    public function getChilds(): Collection
    {
        return $this->childs;
    }

    public function addChild(Relation $relation): self
    {
        if (!$this->childs->contains($relation)) {
            $this->childs->add($relation);
            $relation->setParent($this);
        }

        return $this;
    }

    public function removeChild(Relation $relation): self
    {
        if ($this->childs->removeElement($relation)) {
            // set the owning side to null (unless already changed)
            if ($child->getParent() === $this) {
                $relation->setParent(null);
            }
        }

        return $this;
    }

    public function getComments() {
        $comments = new ArrayCollection();
        foreach($this->getContent() as $version) {
            $comments = new ArrayCollection(
                array_merge($comments->toArray(), $version->getComments()->toArray())
            );
        }
        return $comments;
    }

    public function getCurrent(): ContentVersion
    {
        $activeList = $this->getContent()
            ->filter(
                function($content) {
                    return $content->getArchivedAt() === null || $content->getArchivedAt() < $content->getPublishedAt();
                }
            )
        ;
        $current = $activeList->first();
        foreach($activeList as $content) {
            if($content->getPublishedAt() > $current->getPublishedAt())
                $current = $content;
        }
        return $current;
    }
}
