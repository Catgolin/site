<?php

namespace Catgolin\WebPenguin\Entity\Content\ContentTree;

use Catgolin\WebPenguin\Repository\Content\ContentTree\RootRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=RootRepository::class)
 */
class Root extends Node
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    public function getId(): ?int
    {
        return $this->id;
    }
}
