<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210122225204 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP TABLE application_java_script_file');
        $this->addSql('ALTER TABLE node DROP INDEX UNIQ_857FE8455F215334, ADD INDEX IDX_857FE8455F215334 (default_link_id)');
        $this->addSql('ALTER TABLE node ADD discr VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE application_java_script_file (application_id INT NOT NULL, java_script_file_id INT NOT NULL, INDEX IDX_219C04C73E030ACD (application_id), INDEX IDX_219C04C755C8B3B5 (java_script_file_id), PRIMARY KEY(application_id, java_script_file_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE node DROP INDEX IDX_857FE8455F215334, ADD UNIQUE INDEX UNIQ_857FE8455F215334 (default_link_id)');
        $this->addSql('ALTER TABLE node DROP discr');
    }
}
