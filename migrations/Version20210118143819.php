<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210118143819 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE child (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, element_id INT NOT NULL, place INT DEFAULT NULL, INDEX IDX_22B35429727ACA70 (parent_id), INDEX IDX_22B354291F1F2A24 (element_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE child ADD CONSTRAINT FK_22B35429727ACA70 FOREIGN KEY (parent_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE child ADD CONSTRAINT FK_22B354291F1F2A24 FOREIGN KEY (element_id) REFERENCES node (id)');
        $this->addSql('DROP TABLE children');
        $this->addSql('ALTER TABLE node DROP FOREIGN KEY FK_857FE845727ACA70');
        $this->addSql('DROP INDEX IDX_857FE845727ACA70 ON node');
        $this->addSql('ALTER TABLE node DROP parent_id');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE children (id INT AUTO_INCREMENT NOT NULL, parent_id INT NOT NULL, element_id INT NOT NULL, place INT DEFAULT NULL, INDEX IDX_A197B1BA1F1F2A24 (element_id), INDEX IDX_A197B1BA727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB COMMENT = \'\' ');
        $this->addSql('ALTER TABLE children ADD CONSTRAINT FK_A197B1BA1F1F2A24 FOREIGN KEY (element_id) REFERENCES node (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE children ADD CONSTRAINT FK_A197B1BA727ACA70 FOREIGN KEY (parent_id) REFERENCES node (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('DROP TABLE child');
        $this->addSql('ALTER TABLE node ADD parent_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE node ADD CONSTRAINT FK_857FE845727ACA70 FOREIGN KEY (parent_id) REFERENCES node (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_857FE845727ACA70 ON node (parent_id)');
    }
}
