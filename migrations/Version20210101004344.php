<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210101004344 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE content_version (id INT AUTO_INCREMENT NOT NULL, origin_id INT DEFAULT NULL, author_id INT DEFAULT NULL, node_id INT NOT NULL, created_at DATETIME NOT NULL, published_at DATETIME DEFAULT NULL, archived_at DATETIME DEFAULT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_60140A7056A273CC (origin_id), INDEX IDX_60140A70F675F31B (author_id), INDEX IDX_60140A70460D9FD7 (node_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE link (id INT AUTO_INCREMENT NOT NULL, target_id INT NOT NULL, name VARCHAR(100) NOT NULL, INDEX IDX_36AC99F1158E0B66 (target_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE node (id INT AUTO_INCREMENT NOT NULL, owner_id INT DEFAULT NULL, parent_id INT DEFAULT NULL, is_draft TINYINT(1) NOT NULL, INDEX IDX_857FE8457E3C61F9 (owner_id), INDEX IDX_857FE845727ACA70 (parent_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT NOT NULL, username VARCHAR(180) NOT NULL, roles JSON NOT NULL, password VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_8D93D649F85E0677 (username), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE content_version ADD CONSTRAINT FK_60140A7056A273CC FOREIGN KEY (origin_id) REFERENCES content_version (id)');
        $this->addSql('ALTER TABLE content_version ADD CONSTRAINT FK_60140A70F675F31B FOREIGN KEY (author_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE content_version ADD CONSTRAINT FK_60140A70460D9FD7 FOREIGN KEY (node_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE link ADD CONSTRAINT FK_36AC99F1158E0B66 FOREIGN KEY (target_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE node ADD CONSTRAINT FK_857FE8457E3C61F9 FOREIGN KEY (owner_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE node ADD CONSTRAINT FK_857FE845727ACA70 FOREIGN KEY (parent_id) REFERENCES node (id)');
        $this->addSql('ALTER TABLE user ADD CONSTRAINT FK_8D93D649BF396750 FOREIGN KEY (id) REFERENCES content_version (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE content_version DROP FOREIGN KEY FK_60140A7056A273CC');
        $this->addSql('ALTER TABLE user DROP FOREIGN KEY FK_8D93D649BF396750');
        $this->addSql('ALTER TABLE content_version DROP FOREIGN KEY FK_60140A70F675F31B');
        $this->addSql('ALTER TABLE content_version DROP FOREIGN KEY FK_60140A70460D9FD7');
        $this->addSql('ALTER TABLE link DROP FOREIGN KEY FK_36AC99F1158E0B66');
        $this->addSql('ALTER TABLE node DROP FOREIGN KEY FK_857FE8457E3C61F9');
        $this->addSql('ALTER TABLE node DROP FOREIGN KEY FK_857FE845727ACA70');
        $this->addSql('DROP TABLE content_version');
        $this->addSql('DROP TABLE link');
        $this->addSql('DROP TABLE node');
        $this->addSql('DROP TABLE user');
    }
}
