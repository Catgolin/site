<?php

namespace Catgolin\Tests\Content;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentTree\Root;
use Catgolin\WebPenguin\Entity\Content\ContentType\Category;
use Catgolin\WebPenguin\Entity\Content\ContentType\Post;
use Catgolin\WebPenguin\Entity\Content\ContentType\User;
use Catgolin\WebPenguin\Repository\Content\ContentType\CategoryRepository;
use Catgolin\WebPenguin\Repository\Content\ContentType\PostRepository;

use Catgolin\Tests\LoginTrait;

use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use PHPUnit\Framework\TestCase;

class CategoryTest extends WebTestCase
{
    use FixturesTrait;
    use LoginTrait;

    /**
     * @var KernelBrowser
     **/
    private $client;
    /**
     * @var array
     **/
    private $fixtures;
    /**
     * @var User
     **/
    private $user;
    /**
     * @var string
     **/
    private $password;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->fixtures = $this->loadFixtureFiles([
            __DIR__ . '/../Fixtures/UserFixtures.yaml',
            __DIR__ . '/../Fixtures/CategoryFixtures.yaml',
        ]);
        $this->user = $this->fixtures['user1'];
        $this->password = 'password';
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testCreationDisplay(callable $parent): Crawler
    {
        // Preparation
        $parent = $parent($this);
        $this->user = $parent->getAuthor()->getCurrent();
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $crawler = $this->client->request('GET', '/categories/c/' . $destination);
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("#edit_category_publish");
        return $crawler;
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testForbidCreationUnrelated(callable $parent)
    {
        // Preparation
        $parent = $parent($this);
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/categories/c/' . $destination
        );
        // Verification
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testDenyCreationAnonymous(callable $parent)
    {
        $parent = $parent($this);
        // Action
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/categories/c/' . $destination
        );
        $this->assertResponseRedirects('/login');
    }

    /**
     * @depends testCreationDisplay
     */
    public function testRootCreationDisplay(): Crawler
    {
        // Preparation
        $this->user->setRoles(['ROLE_ADMIN']);
        $entityManager = $this->client
            ->getContainer()
            ->get('doctrine')
            ->getManager();
        $entityManager->persist($this->user);
        $entityManager->flush();
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $crawler = $this->client->request('GET', '/categories/c');
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("#edit_category_publish");
        return $crawler;
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $content
     */
    public function testEditionDisplay(callable $content): Crawler
    {
        // Preparation
        $content = $content($this);
        $this->user = $content->getAuthor()->getCurrent();
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $crawler = $this->client->request('GET', '/categories/e/' . $destination);
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("#edit_category_publish");
        $form = $crawler->filter("#edit_category_publish")
            ->form()
            ->getValues()
        ;
        $this->assertEquals(
            $form['edit_category[description]'],
            $content->getDescription()
        );
        $this->assertEquals(
            $form['edit_category[title]'],
            $content->getTitle()
        );
        $this->assertSelectorExists("#edit_post_publish");
        $this->assertSelectorExists("a[href*='/categories/c/" . $destination . "']");
        return $crawler;
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testForbidEditionUnrelated(callable $content)
    {
        // Preparation
        $content = $content($this);
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/categories/e/' . $destination
        );
        // Verification
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testDenyEditionAnonymous(callable $content)
    {
        $content = $content($this);
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/categories/e/' . $destination
        );
        $this->assertResponseRedirects('/login');
    }

    /**
    * @dataProvider categoryProvider
    * @param callable|Category $content
    */
    public function testView(callable $content): Crawler
    {
        // Preparation
        $content = $content($this);
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $crawler = $this->client->request('GET', '/categories/v/' . $destination);
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString(
            $content->getTitle(),
            $this->client->getResponse()->getContent()
        );
        return $crawler;
    }

    /**
     * @depends testCreationDisplay
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testCreate(callable $parent): Category
    {
        // Preparation
        $parent = $parent($this);
        $faker = Factory::create();
        $title = $faker->words(2, true);
        $description = $faker->text;
        $crawler = $this->testCreationDisplay(
            function() use ($parent) {
                return $parent;
            }
        );
        // Action
        $this->client->submitForm('edit_category[publish]', [
            'edit_category[title]' => $title,
            'edit_category[description]' => $description
        ]);
        // Getting results
        $category = static::$container->get(CategoryRepository::class)
            ->findOneByDescription($description);
        $parent = static::$container->get(CategoryRepository::class)
            ->findOneByDescription($parent->getDescription());
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($category);
        $link = $category->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/categories/e/' . $link->getName(),
            Response::HTTP_CREATED
        );
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $category->getAuthor()->getId()
        );
        $this->assertContains(
            $category->getNode()->getId(),
            $parent->getContent()->map(
                function($relation) {
                    return $relation->getChild()->getId();
                }
            )
        );
        return $category;
    }

    /**
     * @depends testCreate
     * @depends testRootCreationDisplay
     */
    public function testCreateRoot(): Category
    {
        // Preparation
        $faker = Factory::create();
        $title = $faker->words(2, true);
        $description = $faker->text;
        $crawler = $this->testRootCreationDisplay();
        // Action
        $this->client->submitForm('edit_category[publish]', [
            'edit_category[title]' => $title,
            'edit_category[description]' => $description
        ]);
        // Getting results
        $category = static::$container->get(CategoryRepository::class)
            ->findOneByDescription($description);
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($category);
        $link = $category->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/categories/e/' . $link->getName(),
            Response::HTTP_CREATED
        );
        $this->assertTrue($category->getNode() instanceof Root);
        return $category;
    }

    /**
     * @depends testEditionDisplay
     * @dataProvider categoryProvider
     * @param callable|Category $content
     */
    public function testEdit(callable $content): Category
    {
        // Preparation
        $content = $content($this);
        $faker = Factory::create();
        $title = $faker->word(2, true);
        $description = $faker->text;
        // Action
        $crawler = $this->testEditionDisplay(
            function() use ($content) {
                return $content;
            }
        );
        $this->client->submitForm('edit_category[publish]', [
            'edit_category[title]' => $title,
            'edit_category[description]' => $description
        ]);
        // Getting results
        $category = static::$container->get(CategoryRepository::class)
            ->findOneByDescription($description);
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($category);
        $link = $category->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/categories/v/' . $link->getName(),
            Response::HTTP_FOUND
        );
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $category->getAuthor()->getId()
        );
        $this->assertEquals(
            $category->getNode()->getId(),
            $content->getNode()->getId()
        );
        return $category;
    }

    /**
     * @depends testEditionDisplay
     * @dataProvider categoryProvider
     * @param callable|Category $content
     */
    public function testCreatePost(callable $content): Post
    {
        // Preparation
        $parent = $content($this);
        $faker = Factory::create();
        $content = $faker->text;
        $crawler = $this->testEditionDisplay(
            function() use ($parent) {
                return $parent;
            }
        );
        // Action
        $this->client->submitForm('edit_post[publish]', [
            'edit_post[content]' => $content
        ]);
        // Getting results
        $post = static::$container->get(PostRepository::class)
            ->findOneByContent($content);
        $parent = static::$container->get(CategoryRepository::class)
            ->findOneByDescription($parent->getDescription());
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($post);
        $link = $post->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/posts/e/' . $link->getName(),
            Response::HTTP_CREATED
        );
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $post->getAuthor()->getId()
        );
        $this->assertContains(
            $post->getNode()->getId(),
            $parent->getContent()->map(
                function($relation) {
                    return $relation->getChild()->getId();
                }
            )
        );
        return $post;
    }

    public function categoryProvider(): array
    {
        return [
            "Category 1" => [
                function(TestCase $obj) {
                    return $obj->fixtures['category1'];
                }
            ],
            "Category 2" => [
                function(TestCase $obj) {
                    return $obj->fixtures['category2'];
                }
            ]
        ];
    }

}
