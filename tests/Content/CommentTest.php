<?php

namespace Catgolin\Tests\Content;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentType\Category;
use Catgolin\WebPenguin\Entity\Content\ContentType\Post;
use Catgolin\WebPenguin\Entity\Content\ContentType\Comment;
use Catgolin\WebPenguin\Entity\Content\ContentType\User;
use Catgolin\WebPenguin\Repository\Content\ContentType\CommentRepository;
use Catgolin\WebPenguin\Repository\Content\LinkRepository;

use Catgolin\Tests\LoginTrait;

use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use PHPUnit\Framework\TestCase;

class CommentTest extends WebTestCase
{
    use FixturesTrait;
    use LoginTrait;

    /**
     * @var KernelBrowser
     **/
    private $client;
    /**
     * @var array
     **/
    private $fixtures;
    /**
     * @var User
     **/
    private $user;
    /**
     * @var string
     **/
    private $password;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->fixtures = $this->loadFixtureFiles([
            __DIR__ . '/../Fixtures/UserFixtures.yaml',
            __DIR__ . '/../Fixtures/CategoryFixtures.yaml',
            __DIR__ . '/../Fixtures/ApplicationFixtures.yaml',
            __DIR__ . '/../Fixtures/PostFixtures.yaml',
        ]);
        $this->user = $this->fixtures['user1'];
        $this->password = 'password';
    }

    /**
     * @dataProvider contentProvider
     * @param callable|ContentVersion $parent
     */
    public function testCreate(callable $parent, string $path): Comment
    {
        // Preparation
        $parent = $parent($this);
        $faker = Factory::create();
        $content = $faker->text;
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $this->client->request('GET', '/' . $path . '/v/' . $destination);
        // Action
        $this->client->submitForm('comment[publish]', [
            'comment[content]' => $content,
        ]);
        // Getting results
        $parent = static::$container->get(LinkRepository::class)
            ->findOneByName($destination)
            ->getTarget()
            ->getCurrent()
        ;
        $comment = static::$container->get(CommentRepository::class)
            ->findOneByContent($content);
        // Verification
        $this->assertResponseRedirects(
            '/' . $path . '/v/' . $parent->getNode()->getDefaultLink()->getName(),
            Response::HTTP_CREATED
        );
        $this->client->getResponse()->setStatusCode(Response::HTTP_FOUND);
        $this->assertNotNull($comment);
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $comment->getAuthor()->getId()
        );
        $this->assertEquals(
            $comment->getContent(),
            $content
        );
        $this->assertContains(
            $comment->getId(),
            $parent->getComments()->map(
                function($comment) {
                    return $comment->getId();
                }
            ),
            "The comment have not been added to the content"
        );
        $this->client->request('GET', '/' . $path . '/v/' . $destination);
        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString(
            $content,
            $this->client->getResponse()->getContent(),
            "The comment is not displayed on the content's page"
        );
        return $comment;
    }

    public function contentProvider(): array
    {
        return [
            "Category 1" => [
                function(TestCase $obj) {
                    return $obj->fixtures['category1'];
                },
                "categories"
            ],
            "Application 1" => [
                function(TestCase $obj) {
                    return $obj->fixtures['application1'];
                },
                "applications"
            ],
            "Post 1" => [
                function(TestCase $obj) {
                    return $obj->fixtures['post1'];
                },
                "posts"
            ]
        ];
    }

}
