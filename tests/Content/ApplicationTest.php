<?php

namespace Catgolin\Tests\Content;

use Catgolin\WebPenguin\Entity\Content\ContentTree\Node;
use Catgolin\WebPenguin\Entity\Content\ContentTree\Root;
use Catgolin\WebPenguin\Entity\Content\ContentType\Category;
use Catgolin\WebPenguin\Entity\Content\ContentType\Application;
use Catgolin\WebPenguin\Entity\Content\ContentType\JavaScriptFile;
use Catgolin\WebPenguin\Entity\Content\ContentType\User;
use Catgolin\WebPenguin\Repository\Content\ContentType\CategoryRepository;
use Catgolin\WebPenguin\Repository\Content\ContentType\ApplicationRepository;
use Catgolin\WebPenguin\Repository\Content\ContentType\JavaScriptFileRepository;

use Catgolin\Tests\LoginTrait;

use Faker\Factory;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\HttpFoundation\Response;
use PHPUnit\Framework\TestCase;

class ApplicationTest extends WebTestCase
{
    use FixturesTrait;
    use LoginTrait;

    /**
     * @var KernelBrowser
     **/
    private $client;
    /**
     * @var array
     **/
    private $fixtures;
    /**
     * @var User
     **/
    private $user;
    /**
     * @var string
     **/
    private $password;

    public function setUp(): void
    {
        $this->client = static::createClient();
        $this->fixtures = $this->loadFixtureFiles([
            __DIR__ . '/../Fixtures/UserFixtures.yaml',
            __DIR__ . '/../Fixtures/CategoryFixtures.yaml',
            __DIR__ . '/../Fixtures/ApplicationFixtures.yaml',
        ]);
        $this->user = $this->fixtures['user1'];
        $this->password = 'password';
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testCreationDisplay(callable $parent): Crawler
    {
        // Preparation
        $parent = $parent($this);
        $this->user = $parent->getAuthor()->getCurrent();
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $crawler = $this->client->request(
            'GET', '/applications/c/' . $destination
        );
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("#edit_application_publish");
        $this->assertSelectorExists("#edit_application_title");
        $this->assertSelectorExists("#edit_application_description");
        return $crawler;
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testForbidCreationUnrelated(callable $parent)
    {
        // Preparation
        $parent = $parent($this);
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/applications/c/' . $destination
        );
        // Verification
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testDenyCreationAnonymous(callable $parent)
    {
        $parent = $parent($this);
        // Action
        $destination = $parent->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/applications/c/' . $destination
        );
        $this->assertResponseRedirects('/login');
    }

    /**
     * @depends testCreationDisplay
     */
    public function testRootCreationDisplay(): Crawler
    {
        // Preparation
        $this->user->setRoles(['ROLE_ADMIN']);
        $entityManager = $this->client
            ->getContainer()
            ->get('doctrine')
            ->getManager();
        $entityManager->persist($this->user);
        $entityManager->flush();
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $crawler = $this->client->request('GET', '/applications/c');
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("#edit_application_publish");
        $this->assertSelectorExists("#edit_application_title");
        $this->assertSelectorExists("#edit_application_description");
        return $crawler;
    }

    /**
     * @dataProvider applicationProvider
     * @param callable|Application $content
     */
    public function testEditionDisplay(callable $content): Crawler
    {
        // Preparation
        $content = $content($this);
        $this->user = $content->getAuthor()->getCurrent();
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $crawler = $this->client->request(
            'GET', '/applications/e/' . $destination
        );
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists("#edit_application_publish");
        foreach($content->getCode() as $code) {
            $this->assertSelectorExists(
                "#edit_java_script_file_" . $code->getCurrent()->getTitle() . "_publish"
            );
        }
        $this->assertSelectorExists("#create_java_script_file_publish");
        $form = $crawler->filter("#edit_application_publish")
            ->form()
            ->getValues()
        ;
        $this->assertEquals(
            $form['edit_application[description]'],
            $content->getDescription()
        );
        $this->assertEquals(
            $form['edit_application[title]'],
            $content->getTitle()
        );
        return $crawler;
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testForbidEditionUnrelated(callable $content)
    {
        // Preparation
        $content = $content($this);
        $this->login(
            $this->client,
            $this->user->getUsername(),
            $this->password
        );
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/applications/e/' . $destination
        );
        // Verification
        $this->assertResponseStatusCodeSame(Response::HTTP_FORBIDDEN);
    }

    /**
     * @dataProvider categoryProvider
     * @param callable|Category $parent
     */
    public function testDenyEditionAnonymous(callable $content)
    {
        $content = $content($this);
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $this->client->request(
            'GET',
            '/applications/e/' . $destination
        );
        $this->assertResponseRedirects('/login');
    }

    /**
    * @dataProvider applicationProvider
    * @param callable|Application $content
    */
    public function testView(callable $content): Crawler
    {
        // Preparation
        $content = $content($this);
        // Action
        $destination = $content->getNode()->getDefaultLink()->getName();
        $crawler = $this->client->request(
            'GET', '/applications/v/' . $destination
        );
        // Verification
        $this->assertResponseIsSuccessful();
        $this->assertStringContainsString(
            $content->getTitle(),
            $this->client->getResponse()->getContent()
        );
        $this->assertStringContainsString(
            $content->getDescription(),
            $this->client->getResponse()->getContent()
        );
        foreach($content->getCode() as $code) {
            $this->assertStringContainsString(
                $code->getCurrent()->getCode(),
                $this->client->getResponse()->getContent()
            );
        }
        return $crawler;
    }

    /**
     * @depends testCreationDisplay
     * @dataProvider categoryProvider
     * @param callable|Application $parent
     */
    public function testCreate(callable $parent): Application
    {
        // Preparation
        $parent = $parent($this);
        $faker = Factory::create();
        $title = $faker->words(2, true);
        $description = $faker->text;
        $crawler = $this->testCreationDisplay(
            function() use ($parent) {
                return $parent;
            }
        );
        // Action
        $this->client->submitForm('edit_application[publish]', [
            'edit_application[title]' => $title,
            'edit_application[description]' => $description
        ]);
        // Getting results
        $application = static::$container->get(ApplicationRepository::class)
            ->findOneByDescription($description);
        $parent = static::$container->get(CategoryRepository::class)
            ->findOneByDescription($parent->getDescription());
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($application);
        $link = $application->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/applications/e/' . $link->getName(),
            Response::HTTP_CREATED
        );
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $application->getAuthor()->getId()
        );
        $this->assertContains(
            $application->getNode()->getId(),
            $parent->getContent()->map(
                function($relation) {
                    return $relation->getChild()->getId();
                }
            )
        );
        return $application;
    }

    /**
     * @depends testEditionDisplay
     * @dataProvider applicationProvider
     * @param callable|Application $content
     */
    public function testEdit(callable $content): Application
    {
        // Preparation
        $content = $content($this);
        $faker = Factory::create();
        $title = $faker->word(2, true);
        $description = $faker->text;
        // Action
        $crawler = $this->testEditionDisplay(
            function() use ($content) {
                return $content;
            }
        );
        $this->client->submitForm('edit_application[publish]', [
            'edit_application[title]' => $title,
            'edit_application[description]' => $description
        ]);
        // Getting results
        $application = static::$container->get(ApplicationRepository::class)
            ->findOneByDescription($description);
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($application);
        $link = $application->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/applications/v/' . $link->getName(),
            Response::HTTP_FOUND
        );
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $application->getAuthor()->getId()
        );
        $this->assertEquals(
            $application->getNode()->getId(),
            $content->getNode()->getId()
        );
        $this->assertEquals(
            $application->getId(),
            $application->getNode()->getCurrent()->getId()
        );
        $this->assertContains(
            $content->getDescription(),
            $application->getNode()->getContent()->map(function($version) {
                return $version->getDescription();
            }),
            "The previous version of the content was removed or modified"
        );
        return $application;
    }

    /**
     * @depends testEditionDisplay
     * @dataProvider applicationProvider
     * @param callable|Application $content
     */
    public function testCreateJavaScriptFile(callable $content): JavaScriptFile
    {
        // Preparation
        $parent = $content($this);
        $faker = Factory::create();
        $title = $faker->word;
        $code = $faker->text;
        $crawler = $this->testEditionDisplay(
            function() use ($parent) {
                return $parent;
            }
        );
        // Action
        $this->client->submitForm('create_java_script_file[publish]', [
            'create_java_script_file[title]' => $title,
            'create_java_script_file[code]' => $code,
        ]);
        // Getting results
        $jsFile = static::$container->get(JavaScriptFileRepository::class)
            ->findOneByCode($code);
        $parent = static::$container->get(ApplicationRepository::class)
            ->findOneByDescription($parent->getDescription());
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($jsFile);
        $link = $parent->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/applications/e/' . $link->getName(),
            Response::HTTP_CREATED
        );
        $this->assertEquals($jsFile->getTitle(), $title);
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $jsFile->getAuthor()->getId()
        );
        $this->assertEquals(
            $parent->getNode()->getId(),
            $jsFile->getNode()->getOwner()->getId()
        );
        $this->assertContains(
            $jsFile->getNode()->getId(),
            $parent->getCode()->map(
                function($code) {
                    return $code->getId();
                }
            )
        );
        return $jsFile;
    }

    /**
     * @depends testEditionDisplay
     * @dataProvider applicationProvider
     * @param callable|Application $content
     */
    public function testEditJavaScriptFile(callable $content): JavaScriptFile
    {
        // Preparation
        $parent = $content($this);
        $faker = Factory::create();
        $code = $faker->text;
        $title = $faker->word;
        $jsCount = $parent->getCode()->count();
        $victim_index = \random_int(0, $jsCount-1);
        $victim = $parent->getCode()[$victim_index]->getCurrent();
        $oldCode = $victim->getCode();
        $crawler = $this->testEditionDisplay(
            function() use ($parent) {
                return $parent;
            }
        );
        // Action
        $this->client->submitForm(
            'edit_java_script_file_' . $victim->getTitle() . '[publish]', [
            'edit_java_script_file_' . $victim->getTitle() . '[code]' => $code,
            'edit_java_script_file_' . $victim->getTitle() . '[title]' => $title
        ]);
        // Getting results
        $jsFile = static::$container->get(JavaScriptFileRepository::class)
            ->findOneByCode($code);
        $parent = static::$container->get(ApplicationRepository::class)
            ->findOneByDescription($parent->getDescription());
        // Verification
        $this->assertResponseRedirects();
        $this->assertNotNull($jsFile);
        $link = $parent->getNode()->getDefaultLink();
        $this->assertResponseRedirects(
            '/applications/e/' . $link->getName(),
            Response::HTTP_FOUND
        );
        $this->assertEquals($jsFile->getTitle(), $title);
        $this->assertEquals(
            $this->user->getNode()->getId(),
            $jsFile->getAuthor()->getId()
        );
        $this->assertEquals($jsFile->getCode(), $code);
        $this->assertEquals($parent->getCode()->count(), $jsCount);
        $this->assertContains(
            $code,
            $parent->getCode()[$victim_index]->getContent()->map(
                function($content) {
                    return $content->getCode();
                }
            ),
            "The code have not been added to the application"
        );
        $this->assertContains(
            $oldCode,
            $jsFile->getNode()->getContent()->map(
                function($content) {
                    return $content->getCode();
                }
            ),
            "The old version of the code have been deleted"
        );
        $this->assertEquals(
            $oldCode,
            $jsFile->getOrigin()->getCode(),
            "The old version of the code have been altered"
        );
        $this->assertEquals(
            $code,
            $parent->getCode()[$victim_index]->getCurrent()->getCode(),
            "The current version of the code have not been updated"
        );
        return $jsFile;
    }

    public function categoryProvider(): array
    {
        return [
            "Category 1" => [
                function(TestCase $obj) {
                    return $obj->fixtures['category1'];
                }
            ],
            "Category 2" => [
                function(TestCase $obj) {
                    return $obj->fixtures['category2'];
                }
            ]
        ];
    }

    public function applicationProvider(): array
    {
        return[
            "Application 1" => [
                function(TestCase $obj) {
                    return $obj->fixtures['application1'];
                }
            ],
            "Application 2" => [
                function(TestCase $obj) {
                    return $obj->fixtures['application2'];
                }
            ],
        ];
    }

}
