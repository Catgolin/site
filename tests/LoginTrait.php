<?php

namespace Catgolin\Tests;

use Catgolin\WebPenguin\Entity\Content\ContentType\User;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;

trait LoginTrait
{
    public function login(KernelBrowser $client, string $username, string $password)
    {
        $form = $client->request('GET', '/login')
            ->filter('#login-form')
            ->form()
        ;
        $client->submit($form, [
            'username' => $username,
            'password' => $password
        ]);
    }
}
