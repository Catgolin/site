import './styles/app.scss';

const $ = require('jquery');
const marked = require('marked');
Array.from(document.getElementsByClassName('markdown')).forEach(
     (element) => {
         element.innerHTML = marked(element.innerHTML);
     }
 )

// Code environment
import '../node_modules/codemirror/lib/codemirror.css';
require([
    'codemirror/lib/codemirror', 'codemirror/mode/javascript/javascript'
], function(CodeMirror) {
    Array.from(document.getElementsByClassName("code-input")).forEach(
        function(element) {
            CodeMirror.fromTextArea(element, {
                mode: 'javascript',
                lineNumbers: true,
                indentUnit: 4,
                indentWithTabs: true,
            });
        }
    );
});

// Toggle notification menu
const notificationMenu = document.getElementById("notifications").children[0];
function changeNotificationLink() {
    if(window.location.hash == "#notifications") {
        notificationMenu.href = "#";
    } else {
        notificationMenu.href = "#notifications";
    }
}
window.addEventListener("hashchange", changeNotificationLink); 
changeNotificationLink();

// Update link to parent element
// Gets a.parent-link adjacent to selector.parent-selector
document.getElementsByClassName("parent-selector").forEach(selector => {
    const parentLink = selector.parentElement.querySelector(".parent-link");
    if(parentLink == null) return;
    selector.addEventListener("onChange", function() {
        parentLink.href = selector.options[selector.selectedIndex].value;
    });
});
